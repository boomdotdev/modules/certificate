import EventEmitter from "eventemitter3";
import * as THREE from "three";
import { random } from "./utils";
import gsap from "gsap";

export default class Certificate extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor({ id, name, xp, options = {}, activity }) {
    super();

    this._options = Object.assign(
      {
        certificate: {
          color: "#808080",
          width: 500,
          height: 700,
          thickness: 5,
          rows: 10,
          speed: 0.003,
        },
        activity: {
          color: "#ffffff",
          width: 40.5,
          height: 40.5,
          thickness: 10,
          padding: 5,
          maxXp: 500,
        },
        camera: {
          distance: 30,
          position: {
            z: 2000,
            x: 0,
            y: 0,
          },
        },
        spotLight: {
          color: "#6bd6ff",
          offset: 400,
          speed: 0.9,
        },
        pointLight: {
          color: "#a20aff",
          count: 5,
        },
        star: {
          speed: 3,
          count: 1000,
          color: "#ffffff",
        },
      },
      options
    );

    this._id = id;
    this._name = name;
    this._xp = xp;

    this._scene = null;
    this._camera = null;
    this._renderer = null;
    this._certificate = null;
    this._stars = [];
    this._lights = [];
    this._activitiyLights = [];
    this._activity = this._mergeActivity(activity);

    this.addScene();
    this.addCertificate();
    this.addSpotLights();
    this.addStars();
    this.render();
  }

  get canvas() {
    return this._renderer && this._renderer.domElement;
  }

  setSize(width, height) {
    this._camera.aspect = width / height;
    this._camera.updateProjectionMatrix();
    this._renderer.setSize(width, height);
  }

  addScene() {
    this._scene = new THREE.Scene();
    this._camera = new THREE.PerspectiveCamera(
      this._options.camera.distance,
      window.innerWidth / window.innerHeight,
      1,
      5000
    );
    this._camera.position.z = this._options.camera.position.z;
    this._camera.position.x = this._options.camera.position.x;
    this._camera.position.y = this._options.camera.position.y;

    this._renderer = new THREE.WebGLRenderer({ alpha: true, antialias: true });
    this._renderer.setSize(window.innerWidth, window.innerHeight);
    this._renderer.shadowMap.enabled = true;
    this._renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    this._renderer.setClearColor(0xffffff, 0);
    this._renderer.shadowMapSoft = true;
    this._renderer.autoClear = false;
  }

  addSpotLights() {
    const lights = [];
    const helper = [];

    const props = [
      [this._options.spotLight.color, 1, 2000, 128, 0],
      [this._options.spotLight.color, 1, 2000, 120, 0, 1],
      [this._options.spotLight.color, 1, 2000, 100, 0, 1],
      [this._options.spotLight.color, 0.8, 2500, 128, 0, 1],
    ];
    const positions = [
      [0, 800, 800],
      [700, 1000, 1000],
      [700, 1000, -1000],
      [0, -1300, 1200],
    ];

    for (let x = 0; x < positions.length; x++) {
      lights[x] = new THREE.SpotLight(...props[x]);
      lights[x].position.set(...positions[x]);
      lights[x].castShadow = true;
      this._scene.add(lights[x]);
      lights[x].shadow.mapSize.width = 1024;
      lights[x].shadow.mapSize.height = 1024;

      helper[x] = new THREE.SpotLightHelper(lights[x]);
    }
  }

  addCertificate() {
    const bg = new THREE.Mesh(
      new THREE.BoxGeometry(
        this._options.certificate.width,
        this._options.certificate.height,
        this._options.certificate.thickness
      ),
      new THREE.MeshStandardMaterial({
        color: this._options.certificate.color,
        roughness: 0.1,
        metalness: 0,
        opacity: 0,
        reflectivity: 0.2,
        envMapIntensity: 0.9,
        transmission: 0.5,
      })
    );
    bg.receiveShadow = true;
    bg.castShadow = true;

    this._certificate = new THREE.Group();
    this._certificate.add(bg);

    this._activity.forEach((item, i) => {
      const row = Math.floor(i / this._options.certificate.rows);
      const activity = this.addActivity(item.xp);
      activity.position.x =
        (this._options.activity.width + this._options.activity.padding) * i - // The item position
        this._options.certificate.width / 2 - // Move it because its anchor is in the center
        this._options.certificate.rows *
          (this._options.activity.width + this._options.activity.padding) *
          row + // Move each row based on the row count
        (this._options.activity.width + this._options.activity.padding); // Add this._options.activity.padding to the edges
      activity.position.y =
        this._options.certificate.height / 2 -
        row * (this._options.activity.height + this._options.activity.padding) -
        (this._options.activity.height + this._options.activity.padding);

      if (activity.material.opacity >= 1) {
        const light = new THREE.PointLight("#DBF6FF", 2, 50);
        light.position.x = activity.position.x;
        light.position.y = activity.position.y;
        light.position.z = 10;
        this._activitiyLights.push(light);
        this._certificate.add(light);
      }

      this._certificate.add(activity);
    });

    const badge = new THREE.Mesh(
      new THREE.BoxGeometry(130, 55, 3),
      new THREE.MeshStandardMaterial({
        color: "#ffffff",
        opacity: 0,
      })
    );
    badge.position.x = -184;
    badge.position.y = -225;
    badge.position.z = 3;
    this._certificate.add(badge);

    const id = this.addText({ size: 37, text: `#${this._id}`, weight: "bold" });
    id.position.y = -251;
    id.position.x = 25;
    id.position.z = 10;
    this._certificate.add(id);

    const name = this.addText({
      size: 22,
      text: this._name.toUpperCase(),
      weight: "bold",
    });
    name.position.y = -310;
    name.position.x = 25;
    name.position.z = 10;
    this._certificate.add(name);

    const xp = this.addText({
      size: 18,
      text: `${this._xp.toLocaleString()}xp`,
      color: "#FFD600",
    });
    xp.position.y = -335;
    xp.position.x = 25;
    xp.position.z = 10;
    this._certificate.add(xp);

    for (let i = 0; i < this._options.pointLight.count; i++) {
      this.addPointLight();
    }

    gsap.fromTo(
      this._certificate.position,
      { y: -window.innerHeight },
      { y: 0, duration: 3, ease: "expo.out" }
    );

    const rotate = gsap.to(this._certificate.rotation, {
      duration: 40,
      y: Math.PI * 2,
      repeat: -1,
      ease: "none",
    });

    window.addEventListener("keydown", (e) => {
      switch (e.key) {
        case " ":
          rotate.paused(!rotate.paused());
          break;

        case "r":
          rotate.repeat(0);
          rotate.progress(0);
          break;
      }
    });

    this._scene.add(this._certificate);
  }

  addText({ text = "", size = 30, color = "#FFFFFF", weight = "" }) {
    const canvas = document.createElement("canvas");
    canvas.width = 500;
    canvas.height = 100;
    var ctx = canvas.getContext("2d");
    ctx.font = `${weight} ${size}px Laca`;
    ctx.fillStyle = color;
    ctx.fillText(text, 0, 35);
    var texture = new THREE.CanvasTexture(canvas);

    return new THREE.Mesh(
      new THREE.PlaneGeometry(500, 100),
      new THREE.MeshBasicMaterial({
        map: texture,
        transparent: 1,
      })
    );
  }

  addActivity(xp) {
    const percentage =
      Math.min(xp, this._options.activity.maxXp) /
        this._options.activity.maxXp +
      0.05;
    const material = new THREE.MeshStandardMaterial({
      color: this._options.activity.color,
      emissive: this._options.activity.color,
      flatShading: THREE.FlatShading,
      transparent: 1,
      side: THREE.DoubleSide,
      opacity: percentage,
    });
    const geometry = new THREE.BoxGeometry(
      this._options.activity.width,
      this._options.activity.height,
      this._options.activity.thickness
    );
    return new THREE.Mesh(geometry, material);
  }

  addPointLight() {
    const sphere = new THREE.SphereGeometry(0, 16, 8);
    const light = new THREE.PointLight(this._options.pointLight.color, 1, 300);
    light.add(
      new THREE.Mesh(
        sphere,
        new THREE.MeshBasicMaterial({ color: this._options.pointLight.color })
      )
    );
    this._lights.push(light);
    this._scene.add(light);
  }

  addStars() {
    const stars = [];
    const position = {
      x: 0,
      y: 0,
      z: 0,
    };

    const material = new THREE.MeshPhongMaterial({
      color: new THREE.Color(this._options.star.color),
      emissive: new THREE.Color(this._options.star.color),
      shininess: new THREE.Color(this._options.star.color),
      shininess: 100,
      flatShading: THREE.FlatShading,
      opacity: 0.1,
      transparent: 1,
    });

    for (let x = 0; x < this._options.star.count; x++) {
      position.x = random(-(window.innerWidth + 500), window.innerWidth + 500);
      position.y = random(
        -(window.innerHeight + 1000),
        window.innerHeight + 1000
      );
      position.z = random(-1000, 3000);

      stars[x] = new THREE.SphereGeometry(random(0.1, 3), 16);
      this._stars[x] = new THREE.Mesh(stars[x], material);
      this._stars[x].castShadow = true;
      this._stars[x].position.set(position.x, position.y, position.z);
      this._scene.add(this._stars[x]);
    }

    gsap.fromTo(
      material,
      { opacity: 0 },
      { opacity: 1, duration: 3, ease: "linear" }
    );
  }

  render() {
    const time = Date.now() * 0.0005;

    this._lights.forEach((light, i) => {
      const multiplier = (i + 1) / 3;

      light.position.x =
        Math.sin(time * this._options.spotLight.speed * multiplier) *
        this._options.spotLight.offset;
      light.position.y =
        Math.cos((time * this._options.spotLight.speed * multiplier) / 2) *
        this._options.spotLight.offset;
      light.position.z =
        Math.sin(time * this._options.spotLight.speed * multiplier) *
        this._options.spotLight.offset;
    });

    this._activitiyLights.forEach((light, i) => {
      const multiplier = (i + 1) / 5;

      light.position.z =
        Math.sin(time * this._options.spotLight.speed * multiplier) * 20;
    });

    for (let x = 0; x < this._stars.length; x++) {
      this._stars[x].position.z -= this._options.star.speed;

      if (this._stars[x].position.z < -1000) {
        this._stars[x].position.z = random(0, 2500);
      }
    }

    requestAnimationFrame(() => this.render());
    this._renderer.render(this._scene, this._camera);
  }

  _mergeActivity(activity) {
    const base = [];
    for (let i = 0; i < 110; i++) {
      base.push({ xp: 0, date: null });
    }
    return base.map((item, i) => {
      if (activity[i]) {
        item.xp = activity[i].xp;
        item.date = activity[i].date;
      }
      return item;
    });
  }
}

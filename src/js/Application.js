import EventEmitter from "eventemitter3";
import { activity } from "./activity";
import Certificate from "./Certificate";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();

    const wrapper = document.querySelector(".wrapper");

    this._certificate = new Certificate({
      id: 777,
      name: "Hristiyan Ivanov",
      xp: 34134,
      activity,
    });
    window.addEventListener("resize", () =>
      this._certificate.setSize(wrapper.clientWidth, wrapper.clientHeight)
    );
    this._certificate.setSize(wrapper.clientWidth, wrapper.clientHeight);
    wrapper.appendChild(this._certificate.canvas);
  }
}

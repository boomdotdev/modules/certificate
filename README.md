![banner.png](https://boomcdn.fra1.digitaloceanspaces.com/43604e5fe0dedb1142f3a46cf1646c71.png)

## 🤘 Install

```bash
$ npm i boom/certificate
```

## 🤓 Usage

```javascript
import Certificate from "certificate";

const container = document.querySelector(".container");

this._certificate = new Certificate({
  id: 777,
  name: "John Doe",
  xp: 777,
  activity: [
    {
      date: "Sat Dec 11 2021",
      xp: 100,
    },
  ],
});
window.addEventListener("resize", () =>
  this._certificate.setSize(container.clientWidth, container.clientHeight)
);
this._certificate.setSize(container.clientWidth, container.clientHeight);
container.appendChild(this._certificate.canvas);
```

### Arguments

| Name     | Type   | Description                                                         |
| -------- | ------ | ------------------------------------------------------------------- |
| id       | number | The id of the minted certificate                                    |
| name     | string | The full name of the user                                           |
| xp       | number | The xp of the user                                                  |
| activity | array  | The activity for each day containing `{ date: string, xp: number }` |
| options  | object | Options object (see more below)                                     |

### Options

```javascript
{
  certificate: {
    color: "#808080",
    width: 500,
    height: 700,
    thickness: 5,
    rows: 10,
    speed: 0.003,
  },
  activity: {
    color: "#ffffff",
    width: 40.5,
    height: 40.5,
    thickness: 10,
    padding: 5,
    maxXp: 300,
  },
  camera: {
    distance: 30,
    position: {
      z: 2000,
      x: 0,
      y: 0,
    },
  },
  spotLight: {
    color: "#6bd6ff",
    offset: 400,
    speed: 0.9,
  },
  pointLight: {
    color: "#a20aff",
    count: 5,
  },
  star: {
    speed: 3,
    count: 1000,
    color: "#ffffff",
  }
}
```

## 👩‍🚀 Developing

The main project file is `src/js/Certificate.js` and it is initialized in `src/js/Application.js` for easier testing.

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:8080
$ npm run start
```
